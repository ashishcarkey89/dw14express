import express, { json } from "express";
import firstRouter from "./src/router/firstRouter.js";
import bikeRouter from "./src/router/bikeRouter.js";
import traineeRouter from "./src/router/traineeRouter.js";
import productRouter from "./src/router/productRouter.js";
import connectToMongoDb from "./src/connectToDb.js";
import teacherRouter from "./src/router/teacherRouter.js";
import userRouter from "./src/router/userRouter.js";
import reviewRouter from "./src/router/reviewRouter.js";
import adminRouter from "./src/router/adminRouter.js";
import fileRouter from "./src/router/fileRouter.js";
import collegeRouter from "./src/router/collegeRouter.js";
import cors from "cors";
import { config } from "dotenv";
config()


//making express application
let expressApp=express();
expressApp.use(cors());
expressApp.use(json());
expressApp.use(express.static("./public"));

expressApp.listen(8000,()=>{   //creating server throught
    console.log("application is listening at port 8000");
    })

//defining seperate app handeler for mananging response for the requests
expressApp.use("/name",firstRouter);    
expressApp.use("/bike",bikeRouter);             // /=localhost:8000/bike
expressApp.use("/trainee",traineeRouter);
expressApp.use("/product",productRouter); 
expressApp.use("/teachers",teacherRouter); 
expressApp.use("/user",userRouter); 
expressApp.use("/review",reviewRouter); 
expressApp.use("/admin",adminRouter); 
expressApp.use("/file",fileRouter); 
expressApp.use("/college",collegeRouter); 

connectToMongoDb(); //connectimng to db

