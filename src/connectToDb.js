import mongoose from "mongoose";
import secretInfo from "./utils/constantVariable.js";
let connectToMongoDb=()=>{
   mongoose.connect(secretInfo.dbUrl) ;  //creating database named dw14
   console.log("app is connected to db sucessfully")
}
export default connectToMongoDb;