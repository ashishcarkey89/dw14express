import { sendEmail } from "../utils/sendMail.js";
import { Users } from "../schema/model.js";
import bcrypt from "bcrypt";


export let createUser= async(req, res, next) => {
    let data = req.body;                // while request sending data from (postman)/frontend 
    /* console.log(data);  */
  
    try {
        //sending data to the database 
       data.password=await bcrypt.hash(data.password,10)
       let result = await Users.create(data);
       console.log(data)
     await sendEmail({
        from:'"Ashish"<ashishcarkey@gmail.com>' ,
        to:[data.email],
        subject:"user registration sucessfull",
        html:`<p> ${data.fullName}User registration is sucessfull</p>`
      })
      
      res.status(200).json({ sucess: "true", message: "user created sucesfully",
      result:result
     }); 
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }
  };

  export const userLogin=async(req,res,next)=>{

    let data=req.body;
    console.log(data)
    let email=req.body.email;
    console.log(email);
    const user = await Users.findOne({ email });
    console.log(user)
  
    if (!user) {
      return res.status(404).json({ success: false, message: "Email not found" });
    }
    console.log(req.body.password) //
    console.log(user.password)
    let isValid = await bcrypt.compare(req.body.password, user.password);
    
    if (!isValid) {
      return res.status(401).json({ success: false, message: "Invalid password" });
    }
  
    res.status(200).json({ success: true, message: "User found", user });
  }
  

 /*  export const loginUser = async (req, res, next) => {
    let email = req.body.email;
    let password = req.body.password;
  
    try {
      let user = await Users.findOne({ email: email });//{}or null
  
      if (user === null) {
        res.status(401).json({ success: false, message: "Invalid credention" });
      } else {
        let dbPassword = user.password;
  
        let isValidPassword = await bcrypt.compare(password, dbPassword);
        if (isValidPassword) {
          res.status(200).json({ success: true, message: "login successfully." });
        } else {
          res
            .status(401)
            .json({ success: "false", message: "Invalid credential" });
        }
      }
    } catch (error) {
      res.status(401).json({
        success: false,
        message: "Invalid credential.",
      });
    }
  
    //passsword and dbPassword match  send success response
  }; */







  export let readAllUser=async(req, res, next) => {
    //  let query=req.query;
    //  console.log(query);    //sent from url ? fullname=ashish{ fullName: 'ashish' }
    let {limit,page,...myQuery}=req.query;
    try {

      let result=await Users.find(myQuery).limit(limit).skip((page-1)*limit); //
      //let result=await Users.find(myQuery).sort(sort).select(select); 
      // result=await Users.find({fullName:"ashish"}).select("fullName email");
      // result=await Users.find({fullName:"ashish"}).select("-fullName -password"); //full name rah password bahek aru data fetch
      
      res.json({ message: "true", message: "user read sucesfully",
     result:result
  });
    } catch (error) {
           res.status(400).json({ sucess: "false", message: error.message }); 

    }           
  };


  export let updateSpecificUser=async(req, res, next) => {
  
    try {
      let result=await Users.findByIdAndUpdate(req.params.id,req.body,{new:true,}); 
      res.status(400).json({ message: "true", message: "user read sucesfully",
     result:result
  });
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }      
    }
  export let readSpecificUser=async(req, res, next) => {
    try {
      let result=await Users.findById(req.params.id); 
      res.status(400).json({ message: "true", message: "user read sucesfully",
     result:result
  });
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }         
  };

 export let deleteSpecificUser=async(req, res, next) => {
  
    try {
      let result=await Users.findByIdAndDelete(req.params.id,req.body,{new:true,}); 
      res.status(400).json({ message: "true", message: "user delete sucesfully",
     result:result
  });
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }      
    }



 /*    export let readAllUser=async(req, res, next) => {
      let query=req.query;
      console.log(query);    //sent from url ? fullname=ashish{ fullName: 'ashish' }
     let {sort,select, ...myQuery}=req.query;
     try {
       let result=await Users.find(myQuery).sort(sort).select(select); 
       // result=await Users.find({fullName:"ashish"}).select("fullName email");
       // result=await Users.find({fullName:"ashish"}).select("-fullName -password"); //full name rah password bahek aru data fetch
       
       res.json({ message: "true", message: "user read sucesfully",
      result:result
   });
     } catch (error) {
            res.status(400).json({ sucess: "false", message: error.message }); 

     }           
   };
  */




  // for skiping and showing data
 /*  export let readAllUser=async(req, res, next) => {
    //  let query=req.query;
    //  console.log(query);    //sent from url ? fullname=ashish{ fullName: 'ashish' }
    let {limit,page,...myQuery}=req.query;
    try {

      let result=await Users.find(myQuery).limit(limit).skip((page-1)*limit); //
      //let result=await Users.find(myQuery).sort(sort).select(select); 
      // result=await Users.find({fullName:"ashish"}).select("fullName email");
      // result=await Users.find({fullName:"ashish"}).select("-fullName -password"); //full name rah password bahek aru data fetch
      
      res.json({ message: "true", message: "user read sucesfully",
     result:result
  });
    } catch (error) {
           res.status(400).json({ sucess: "false", message: error.message }); 

    }           
  }; */