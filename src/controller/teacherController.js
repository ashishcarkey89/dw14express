import { sendSMS } from "../utils/sendMessage.js";
import { Teachers } from "../schema/model.js";

export let createTeacher = async (req, res, next) => {
  let data = req.body;
  try {
    let result = await Teachers.create(data); // Sending data to the database
    
    // SMS details
    const from = "Vonage APIs";
    const to = "9779849511645"; 
    const text = 'hello aayush badar❤️ ';
    
    // Send SMS
    await sendSMS(to, from, text); // Wait for the SMS to be sent
    
    res.status(200).json({
      success: true,
      message: "Teacher created successfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({ success: false, message: error.message });
  }
};

export let readAllTeacher = async (req, res, next) => {
  try {
    let result = await Teachers.find({});
    res.json({
      message: "true",
      message: "teacher read sucesfully",
      result: result,
    });
  } catch (error) {
    res.json({ sucess: "false", message: error.message });
  }
};

export let updateSpecificTeacher = async (req, res, next) => {
  try {
    let result = await Teachers.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    res
      .status(400)
      .json({
        message: "true",
        message: "teacher read sucesfully",
        result: result,
      });
  } catch (error) {
    res.status(400).json({ sucess: "false", message: error.message });
  }
};
export let readSpecificTeacher = async (req, res, next) => {
  try {
    let result = await Teachers.findById(req.params.id);
    res
      .status(400)
      .json({
        message: "true",
        message: "teacher read sucesfully",
        result: result,
      });
  } catch (error) {
    res.status(400).json({ sucess: "false", message: error.message });
  }
};

export let deleteSpecificTeacher = async (req, res, next) => {
  try {
    let result = await Teachers.findByIdAndDelete(req.params.id, req.body, {
      new: true,
    });
    res
      .status(400)
      .json({
        message: "true",
        message: "teacher delete sucesfully",
        result: result,
      });
  } catch (error) {
    res.status(400).json({ sucess: "false", message: error.message });
  }
};
