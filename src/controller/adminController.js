// import { Admins } from "../schema/model.js";

import { Admins } from "../schema/model.js";

export let createAdmin= async(req, res, next) => {
    let data = req.body;                // while request sending data from (postman)/frontend 
    /* console.log(data);  */
    
    try {
      let result = await Admins.create(data);  //sending data to the database 
      res.status(200).json({ sucess: "true", message: "admin created sucesfully",
      result:result
     }); 
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }
  };

  export let readAllAdmin=async(req, res, next) => {
    //  let query=req.query;
    //  console.log(query);    //sent from url ? fullname=ashish{ fullName: 'ashish' }
    let {limit,page,...myQuery}=req.query;
    try {

      let result=await Admins.find(myQuery).limit(limit).skip((page-1)*limit); //
      //let result=await Admins.find(myQuery).sort(sort).select(select); 
      // result=await Admins.find({fullName:"ashish"}).select("fullName email");
      // result=await Admins.find({fullName:"ashish"}).select("-fullName -password"); //full name rah password bahek aru data fetch
      
      res.json({ message: "true", message: "admin read sucesfully",
     result:result
  });
    } catch (error) {
           res.status(400).json({ sucess: "false", message: error.message }); 

    }           
  };


  export let updateSpecificAdmin=async(req, res, next) => {
  
    try {
      let result=await Admins.findByIdAndUpdate(req.params.id,req.body,{new:true,}); 
      res.status(400).json({ message: "true", message: "admin read sucesfully",
     result:result
  });
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }      
    }
  export let readSpecificAdmin=async(req, res, next) => {
    try {
      let result=await Admins.findById(req.params.id); 
      res.status(400).json({ message: "true", message: "admin read sucesfully",
     result:result
  });
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }         
  };

 export let deleteSpecificAdmin=async(req, res, next) => {
  
    try {
      let result=await Admins.findByIdAndDelete(req.params.id,req.body,{new:true,}); 
      res.status(400).json({ message: "true", message: "admin delete sucesfully",
     result:result
  });
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }      
    }



 /*    export let readAllAdmin=async(req, res, next) => {
      let query=req.query;
      console.log(query);    //sent from url ? fullname=ashish{ fullName: 'ashish' }
     let {sort,select, ...myQuery}=req.query;
     try {
       let result=await Admins.find(myQuery).sort(sort).select(select); 
       // result=await Admins.find({fullName:"ashish"}).select("fullName email");
       // result=await Admins.find({fullName:"ashish"}).select("-fullName -password"); //full name rah password bahek aru data fetch
       
       res.json({ message: "true", message: "admin read sucesfully",
      result:result
   });
     } catch (error) {
            res.status(400).json({ sucess: "false", message: error.message }); 

     }           
   };
  */




  // for skiping and showing data
 /*  export let readAllAdmin=async(req, res, next) => {
    //  let query=req.query;
    //  console.log(query);    //sent from url ? fullname=ashish{ fullName: 'ashish' }
    let {limit,page,...myQuery}=req.query;
    try {

      let result=await Admins.find(myQuery).limit(limit).skip((page-1)*limit); //
      //let result=await Admins.find(myQuery).sort(sort).select(select); 
      // result=await Admins.find({fullName:"ashish"}).select("fullName email");
      // result=await Admins.find({fullName:"ashish"}).select("-fullName -password"); //full name rah password bahek aru data fetch
      
      res.json({ message: "true", message: "admin read sucesfully",
     result:result
  });
    } catch (error) {
           res.status(400).json({ sucess: "false", message: error.message }); 

    }           
  }; */