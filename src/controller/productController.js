//In controller file we write request handeling functions and use them router files.
import { Products } from "../schema/model.js";

export let createProduct= async(req, res, next) => {
    let data = req.body;                                // Getting requesting we sending data from (postman)/frontend 
    /* console.log(data);  */
    try {
      let result = await Products.create(data);         //sending received data to the database 
      res.status(200).json({ sucess: "true", message: "product created sucesfully",
      result:result
     }); 
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }
  };

  export let readAllProduct=async(req, res, next) => {
    
    try {
      let result=await Products.find({}); 
      res.json({ message: "true", message: "product read sucesfully",
      result:result                                                       //shows request activity data responses in postman terminal 
  });
    } catch (error) {
           res.status(400).json({ sucess: "false", message: error.message }); 

    }           
  };

  export let readSpecificProduct=async(req, res, next) => {
    try {
      let result=await Products.findById(req.params.id); 
      res.status(400).json({ message: "true", message: "product read sucesfully",
     result:result
  });
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }         
  };

  export let updateSpecificProduct=async(req, res, next) => {
  
    try {
      let result=await Products.findByIdAndUpdate(req.params.id,req.body,{new:true,}); 
      res.status(400).json({ message: "true", message: "product read sucesfully",
     result:result
  });
    } catch (error) {
           res.status(400).json({ sucess: "false", message: error.message }); 

    }      
    }
  

 export let deleteSpecificProduct=async(req, res, next) => {
    try {
      let result=await Products.findByIdAndDelete(req.params.id,req.body,{new:true,}); 
      res.status(400).json({ message: "true", message: "product delete sucesfully",
     result:result
  });
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }      
    }