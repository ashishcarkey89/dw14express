import { Reviews } from "../schema/model.js";

export let createReview= async(req, res, next) => {
    let data = req.body;                // while request sending data from (postman)/frontend 
    /* console.log(data);  */
    
    try {
      let result = await Reviews.create(data);  //sending data to the database 
      res.status(200).json({ sucess: "true", message: "review created sucesfully",
      result:result
     }); 
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }
  };

  export let readAllReview=async(req, res, next) => {
    try {
      let result=await Reviews.find({}).populate("product").populate("user");  //It means populate displays the details of product and user defined in reviews details
      res.json({ message: "true", message: "review read sucesfully",
      result:result
  });
    } catch (error) {
           res.status(400).json({ sucess: "false", message: error.message }); 

    }           
  };

  
  export let updateSpecificReview=async(req, res, next) => {
    try {
      let result=await Reviews.findByIdAndUpdate(req.params.id,req.body,{new:true,}); //new means latest update data gets displayed
      res.status(400).json({ message: "true", message: "review read sucesfully",
      result:result
  });
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }      
    }
  export let readSpecificReview=async(req, res, next) => {
    try {
      let result=await Reviews.findById(req.params.id); 
      res.status(400).json({ message: "true", message: "review read sucesfully",
      result:result
  });
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }         
  };

 export let deleteSpecificReview=async(req, res, next) => {
  
    try {
      let result=await Reviews.findByIdAndDelete(req.params.id,req.body,{new:true,});    //new means latest deleted data gets displayed
      res.status(400).json({ message: "true", message: "review delete sucesfully",
      result:result
  });
    } catch (error) {
      res.status(400).json({ sucess: "false", message: error.message }); 
    }      
    }