//import collegeSchema from "../schema/collegeSchema.js";
import { Colleges } from "../schema/model.js";
import secretInfo from "../utils/constantVariable.js";

export let createCollege = async (req, res, next) => {
  let data = req.body; // while request sending data from (postman)/frontend
  /* console.log(data);  */

  let link = `${secretInfo.server_Url}/${req.file.filename}`;
  data.Images = link;
  try {
    let result = await Colleges.create(data); //sending data to the database

    res.status(200).json({
      sucess: "true",
      message: "college created sucesfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({ sucess: "false", message: error.message });
  }
};

export let updateSpecificCollege = async (req, res, next) => {
  try {
    let result = await Colleges.findByIdAndUpdate(
      req.params.id,
      req.file.filename,
      {
        new: true,
      }
    ); //new means latest update data gets displayed
    res.status(400).json({
      message: "true",
      message: "college read sucesfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({ sucess: "false", message: error.message });
  }
};

export let readAllCollege = async (req, res, next) => {
  try {
    let result = await Colleges.find({}).populate("product").populate("user"); //It means populate displays the details of product and user defined in colleges details
    res.json({
      message: "true",
      message: "college read sucesfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({ sucess: "false", message: error.message });
  }
};

export let readSpecificCollege = async (req, res, next) => {
  try {
    let result = await Colleges.findById(req.params.id);
    res.status(400).json({
      message: "true",
      message: "college read sucesfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({ sucess: "false", message: error.message });
  }
};

export let deleteSpecificCollege = async (req, res, next) => {
  try {
    let result = await Colleges.findByIdAndDelete(req.params.id, req.body, {
      new: true,
    }); //new means latest deleted data gets displayed
    res.status(400).json({
      message: "true",
      message: "college delete sucesfully",
      result: result,
    });
  } catch (error) {
    res.status(400).json({ sucess: "false", message: error.message });
  }
};
