import { Router } from "express";
import { createUser, deleteSpecificUser, readAllUser, readSpecificUser, updateSpecificUser, userLogin } from "../controller/userController.js";

let userRouter = Router();

userRouter
  .route("/")
  .post(createUser)
  .get(readAllUser)


  userRouter
  .route("/login")
  .post(userLogin)


  userRouter.route("/:id") //localhost:8000user/id
  .get(readSpecificUser)
.patch(updateSpecificUser)

  .delete(deleteSpecificUser)
  export default userRouter;