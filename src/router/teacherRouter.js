import { Router } from "express";
import { createTeacher, deleteSpecificTeacher, readAllTeacher, readSpecificTeacher, updateSpecificTeacher } from "../controller/teacherController.js";

let teacherRouter = Router();

teacherRouter
  .route("/")
  .post(createTeacher)
  .get(readAllTeacher)

  teacherRouter.route("/:id") //localhost:8000product/id
  .get(readSpecificTeacher)
.patch(updateSpecificTeacher)

  .delete(deleteSpecificTeacher)
  export default teacherRouter;