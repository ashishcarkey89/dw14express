import { Router } from "express";
import { handleMultipleFile, handleSingleFile } from "../controller/fileController.js";
import upload from "../utils/upload.js";

let fileRouter=Router();
fileRouter
.route("/single")  // /=localhost:8000/file/single
.post(upload.single("docs"),handleSingleFile)



fileRouter
.route("/multiple")
.post(upload.array("docs"),handleMultipleFile)


export default fileRouter;