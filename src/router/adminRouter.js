import { Router } from "express";
import {
  createAdmin,
  deleteSpecificAdmin,
  readAllAdmin,
  readSpecificAdmin,
  updateSpecificAdmin,
} from "../controller/adminController.js";
import Joi from "joi";
import validation from "../middleware/validation.js";
//import { createAdmin, deleteSpecificAdmin, readAllAdmin, readSpecificAdmin, updateSpecificAdmin } from "../controller/adminController.js";

let adminRouter = Router();
let randomvalidation = Joi.object()
  .keys({
    name: Joi.string().required().min(3).max(10).messages({
      "any.required":"name is required",    //to print our own understandable error message
      "string.base":"field must  be string",
      "string.min":"field must be atleast 3 characters",
      "string.max":"upto 10 characters can be used",
    }),
    password: Joi.string().required().min(3).max(10),
    phoneNumber: Joi.number().required(),
    age:Joi.number().custom((value,msg)=>{      //custom means if we want to send our own message for the error
        if (value>=18) {
          return true;
        } else {
          return msg.message("age must be greater to join this event")
        }
    }),
    roll: Joi.number(),
    gender:Joi.string().valid("male","female","other"),
    email:Joi.string().required()//.email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } })
    .custom((value,msg)=>{
      let validemail=value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
      console.log(validemail)
      if (validemail) {
        return true;
      } else {
        return msg.message("Email is not valid")
      }
    }),
    password:Joi.string().required().custom((value,msg)=>{
      let validPassword=value.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/)
      if (validPassword) {
        return true;
      } else {
        return msg.message("Password must have at least one lowercase letter, one uppercase letter, one digit, one special character, and be at least 8 characters long.");
    }
       
      
    }),
    isMarried:Joi.boolean().required(),

    //If user is married(true) then spousename is required so we use when
    spouseName: Joi.when(    //depended to isMarried
      "isMarried",{
        is:true,
        then:Joi.string().required(),
        otherwise:Joi.string(),
      }
    ),
    dob:Joi.date().required(),
    location:Joi.object().keys({country:Joi.string().required(),
     exactLocation:Joi.string().required(),
    }),
    favTeacher:Joi.array().items(Joi.string().required()),
    favSubject:Joi.array().items(Joi.object().keys({  bookName:Joi.string().required(),
      bookAuthor:Joi.string().required(),
    }
    ))
  })
  .unknown(true);

adminRouter
  .route("/")
  .post(validation(randomvalidation), createAdmin)
  .get(readAllAdmin);

adminRouter
  .route("/:id") //localhost:8000admin/id
  .get(readSpecificAdmin)
  .patch(updateSpecificAdmin)

  .delete(deleteSpecificAdmin);
export default adminRouter;
