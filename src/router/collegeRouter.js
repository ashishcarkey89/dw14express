import { Router } from "express";
import {
  createCollege,
  deleteSpecificCollege,
  readAllCollege,
  readSpecificCollege,
  updateSpecificCollege,
} from "../controller/collegeController.js";
import upload from "../utils/upload.js";
//import { createCollege, deleteSpecificCollege, readAllCollege, readSpecificCollege, updateSpecificCollege } from "../controller/collegeController.js";

let collegeRouter = Router();

collegeRouter
  .route("/")
  .post(upload.single("Images"), createCollege) //stores images in public folder
  .get(readAllCollege);
//.patch(upload.single("Images"), updateSpecificCollege);

collegeRouter
  .route("/:id") //localhost:8000college/id
  .get(readSpecificCollege)
  .patch(updateSpecificCollege)

  .delete(deleteSpecificCollege);
export default collegeRouter;
