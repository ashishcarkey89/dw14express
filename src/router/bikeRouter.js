import { Router } from "express";

let bikeRouter=Router();
bikeRouter
.route("/")  // /=localhost:8000/bike
.post((req,res,nex)=>{   //to print in postrman console
   
    res.json("bike post"); //to print in postman console
    /* console.log(req.body); */
})
.get((req,res,nex)=>{
    res.json("bike get");
    console.log(req.query)
})
.patch((req,res,nex)=>{
    res.json("bike update");
})
.delete((req,res,nex)=>{
   res.json("bike delete");
})


//for a new path we need to create again
bikeRouter
.route("/a/:id1/name/:id2")     // /=localhost:8000/bike/a/:ram/name/:hari
.post((req,res,nex)=>{
    console.log(req.query)      // query passed in url gets printed in terminal { name: 'nitan', address: 'gagalphedi' }
})
.get((req,res,nex)=>{
    res.json("bike get");
})
.patch((req,res,nex)=>{
    res.json("bike update");
})
.delete((req,res,nex)=>{
   res.json("bike delete");
})
export default bikeRouter;
