import { Router } from "express";
import { createReview, deleteSpecificReview, readAllReview, readSpecificReview, updateSpecificReview } from "../controller/reviewController.js";

let reviewRouter = Router();

reviewRouter
  .route("/")
  .post(createReview)
  .get(readAllReview)

  reviewRouter.route("/:id") //localhost:8000review/id
  .get(readSpecificReview)
.patch(updateSpecificReview)

  .delete(deleteSpecificReview)
  export default reviewRouter;