import { Router } from "express";
let firstRouter = Router();
const getcontroller2=(a,b)=>{
  return   (req, res, next) => {  //middleware
      console.log(`${a}, ${b}`);
  }}
 const getcontroller1=(req,res,next)=>{  //middleware
   return req.body;
   }

firstRouter
  .route("/") 
  .post(
    (req, res, next) => {
      console.log("I am middleware 1");
     /*  res.json("hello 1");     */    /*  can have only one req.json */
      let error=new Error("error 1");
      next(error);
    },
    (err,req, res, next) => {
      console.log("I am error middleware ");
      /* res.json("hello 1"); */
      next();
    },
    (req, res, next) => {
      console.log("I am middleware 2");
      next();
    },
    (req, res, next) => {
      console.log("I am middleware 3");
      
    },(req, res, next) => {
        console.log("I am middleware 4");
        
      }
  )



  //getcontroller1();
  .get(getcontroller2(2,3))   // use this if you want to pass value
  //.get(getcontroller1)
  .patch((req, res, next) => {
    console.log("I am middleware 3");
  })
  .delete((req, res, next) => {
    console.log("I am middleware 4");
  });


export default firstRouter;


// (req, res, next) => {
//   console.log("I am middleware 2");
// }


