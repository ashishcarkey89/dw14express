//In router files we use the handeling functions defined in controllers and also define path
import { Router } from "express";
import { createProduct, deleteSpecificProduct, readAllProduct, readSpecificProduct, updateSpecificProduct } from "../controller/productController.js";

let productRouter = Router();

productRouter
  .route("/")
  .post(createProduct)
  .get(readAllProduct)

  //request handeling using specific id
  productRouter.route("/:id")   //localhost:8000/product/id
  .get(readSpecificProduct)
  .patch(updateSpecificProduct)

  .delete(deleteSpecificProduct)
  export default productRouter;