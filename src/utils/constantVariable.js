import { config } from "dotenv"

config()

let secretInfo={
    secretKey:process.env.SECRET_KEY,
    user:process.env.USER,
    pass:process.env.PASS,
    dbUrl:process.env.DB_URL,
    server_Url:process.env.SERVER_URL,
}

//console.log(secretInfo)

export default secretInfo;