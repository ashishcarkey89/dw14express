import { Schema } from "mongoose";

 let teacherSchema=Schema({
    fullName:{
        type:String,
        required:true,
    },
    home:{
        type:String,
        required:true,
    },
    profileImage:{
        type:String,
        required:true,
    }
 });
 export default teacherSchema;