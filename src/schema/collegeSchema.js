import { Schema } from "mongoose";

let collegeSchema = Schema({
  Name: {
    type: String,
    //means id type of data can be inserted in product
    //means Products table id can only be valid
    required: true,
  },
  Location: {
    type: String,
    required: true,
  },
  Images: {
    type: String,
    required: false,
  },
});
export default collegeSchema;
