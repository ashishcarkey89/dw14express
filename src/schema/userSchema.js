import { Schema } from "mongoose";

 let userSchema=Schema({
    fullName:{
        type:String,
        required:true,
        lowercase:true,
        trim:true
    },
    password:{
        type:String,
        required:true,
    },email:{
        type:String,
        required:true,
        lowercase:true,
        trim:true,
        unique: true
    },
    address:{
        type:String,
        required:false,
        lowercase:true,
        trim:true
    },gender:{
        type:String,
        required:true,
        lowercase:true,
        trim:true
    }
 });
 export default userSchema;