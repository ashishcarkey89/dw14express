import { Schema } from "mongoose";

 let productSchema=Schema({
    name:{
        type:String, 
        required:true,   //manupulations operations
        lowercase:true,
        trim:true
    },
    price:{
        type:Number,
        required:true,
    },quantity:{
        type:Number,
        required:true,
    },
    description:{
        type:String,
        required:false,
    },isAvailable:{
        type:Boolean,
        required:true,
        default:true
    }
 });
 export default productSchema;