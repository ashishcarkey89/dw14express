import { Schema } from "mongoose";

 let adminSchema=Schema(
    {
        name: {
          type: String,
        },
        password: {
          type: String,
          //required: true,
        },
        phoneNumber: {
          type: Number,
          //required: true,
          lowercase: true,
          trim: true,
          // min:[1234567890,"phone number must be greater than 9"],  //lessthan 3 degits
          // max:[99999999,"phone number must be less than 11"],
         
        },
        roll: {
          type: Number,
          //required: true,
          
        },
        isMarried: {
          type: Boolean,
          //required: true,
          trim: true,
        },
        spouseName: {
          type: String,
          //required: true,
          trim: true,
        },
        email: {
          type: String,
          //required: true,
          trim: true,
        },
        gender: {
          type: String,
          //required: true,
          trim: true,

        },
        dob:{
          type:Date,
          //required: true,
          trim: true,
        },
        location:{
            country:{type:String},
            exactLocation:{type:String}
        },
        favTeacher:[
            {type:String}
        ],
        favSubject:[
           {bookName:{type:String}},
           {bookAuthor:{ type: String}}
        ]
      }
 );
 export default adminSchema;