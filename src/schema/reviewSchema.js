import { Schema } from "mongoose";

 let reviewSchema=Schema({
    product:{
        type:Schema.ObjectId,    //means id type of data can be inserted in product
        ref:"Products",          //means Products table id can only be valid
        required:true,
    },
    description:{
        type:String,
        required:true,
    },user:{
        type:Schema.ObjectId,    //means id type of data can be inserted in product
        ref:"Users",             //means Products table id can only be valid
        required:true,
    }
 });
 export default reviewSchema;